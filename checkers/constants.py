import pygame

WIDTH, HEIGHT = 560, 560
ROWS, COLS = 8, 8
SQUARE_SIZE = WIDTH//COLS

#RGB
RED = (255, 0, 0)
WHITE = (255, 255, 255)
GREEN = (10,255,10);
BLACK = (0, 0, 0)
BLUE = (10, 10, 255)
GREY = (128, 128, 128)
CROWN = pygame.transform.scale(pygame.image.load('checkers/crown.png'), (40, 40))

